#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "rsa.h"
#include "prf.h"

/* NOTE: a random composite surviving 10 Miller-Rabin tests is extremely
 * unlikely.  See Pomerance et al.:
 * http://www.ams.org/mcom/1993-61-203/S0025-5718-1993-1189518-9/
 * */
#define ISPRIME(x) mpz_probab_prime_p(x,10)
#define NEWZ(x) mpz_t x; mpz_init(x)
#define BYTES2Z(x,buf,len) mpz_import(x,len,-1,1,0,0,buf)
#define Z2BYTES(buf,len,x) mpz_export(buf,&len,-1,1,0,0,x)

/* utility function for read/write mpz_t with streams: */
int zToFile(FILE* f, mpz_t x)
{
	size_t i,len = mpz_size(x)*sizeof(mp_limb_t);
	unsigned char* buf = malloc(len);
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b = (len >> 8*i) % 256;
		fwrite(&b,1,1,f);
	}
	Z2BYTES(buf,len,x);
	fwrite(buf,1,len,f);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}
int zFromFile(FILE* f, mpz_t x)
{
	size_t i,len=0;
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b;
		/* XXX error check this; return meaningful value. */
		fread(&b,1,1,f);
		len += (b << 8*i);
	}
	unsigned char* buf = malloc(len);
	fread(buf,1,len,f);
	BYTES2Z(x,buf,len);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}

int rsa_keyGen(size_t keyBits, RSA_KEY* K)
{
	rsa_initKey(K);
	/* TODO: write this.  Use the prf to get random byte strings of
	 * the right length, and then test for primality (see the ISPRIME
	 * macro above).  Once you've found the primes, set up the other
	 * pieces of the key ({en,de}crypting exponents, and n=pq). */

	size_t keyBytes;
   	keyBytes = keyBits / CHAR_BIT;

	int primeCount = 0;
	int isPrime = 0;

	while (1)
	{
		unsigned char* buff;
		buff = malloc(keyBytes);
		randBytes(buff, keyBytes); // give me them random p's and q's
		NEWZ(num);
		BYTES2Z(num, buff, keyBytes);

		isPrime = ISPRIME(num);

		if (isPrime > 0) // may be a prime
		{
			primeCount++;
			if (primeCount == 1)
				mpz_set(K->p, num);	// set p
			else
			{
				mpz_set(K->q, num); // set q
				break;
			}
		}
		free(buff);
	}

	mpz_mul(K->n, K->p, K->q); // compute n

	mpz_t phi, pMinusOne, qMinusOne;

	mpz_init(phi);
	mpz_init(qMinusOne);
	mpz_init(pMinusOne);

	mpz_sub_ui(pMinusOne, K->p, 1);
	mpz_sub_ui(qMinusOne, K->q, 1);
	mpz_mul(phi, pMinusOne, qMinusOne); // compute phi (p-1)(q-1)

	mpz_t temp;
	mpz_init(temp);
	unsigned char * tempBuffer = malloc(keyBytes);

	mpz_t tempD;
	mpz_init(tempD);
	mpz_set_ui(tempD, 1);

	while (mpz_cmp(temp, tempD)){
		randBytes(tempBuffer, keyBytes);
		BYTES2Z(K->e, tempBuffer, keyBytes);
		mpz_gcd(temp, K->e, phi); // find e, s.t. (e,phi) = 1
	}

	free(tempBuffer);

	mpz_invert (K->d, K->e, phi); // find d

	return 0;
}

size_t rsa_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
	/* TODO: write this.  Use BYTES2Z to get integers, and then
	 * Z2BYTES to write the output buffer. */
	NEWZ(cipherText);
	NEWZ(num);
	BYTES2Z(num, inBuf, len);
	mpz_powm(cipherText, num, K->e, K->n); /* m^e mod n */
	Z2BYTES(outBuf, len, cipherText);
	return len; /* TODO: return should be # bytes written */
}
size_t rsa_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
	/* TODO: write this.  See remarks above. */
	NEWZ(plainText);
	NEWZ(num);
	BYTES2Z(num, inBuf, len);
	mpz_powm(plainText, num, K->d, K-> n); /*  m^ed mod n where d is the inverse of e */
	Z2BYTES(outBuf, len, plainText);
	return len;
}

size_t rsa_numBytesN(RSA_KEY* K)
{
	return mpz_size(K->n) * sizeof(mp_limb_t);
}

int rsa_initKey(RSA_KEY* K)
{
	mpz_init(K->d); mpz_set_ui(K->d,0);
	mpz_init(K->e); mpz_set_ui(K->e,0);
	mpz_init(K->p); mpz_set_ui(K->p,0);
	mpz_init(K->q); mpz_set_ui(K->q,0);
	mpz_init(K->n); mpz_set_ui(K->n,0);
	return 0;
}

int rsa_writePublic(FILE* f, RSA_KEY* K)
{
	/* only write n,e */
	zToFile(f,K->n);
	zToFile(f,K->e);
	return 0;
}
int rsa_writePrivate(FILE* f, RSA_KEY* K)
{
	zToFile(f,K->n);
	zToFile(f,K->e);
	zToFile(f,K->p);
	zToFile(f,K->q);
	zToFile(f,K->d);
	return 0;
}
int rsa_readPublic(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K); /* will set all unused members to 0 */
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	return 0;
}
int rsa_readPrivate(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K);
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	zFromFile(f,K->p);
	zFromFile(f,K->q);
	zFromFile(f,K->d);
	return 0;
}
int rsa_shredKey(RSA_KEY* K)
{
	/* clear memory for key. */
	mpz_t* L[5] = {&K->d,&K->e,&K->n,&K->p,&K->q};
	size_t i;
	for (i = 0; i < 5; i++) {
		size_t nLimbs = mpz_size(*L[i]);
		if (nLimbs) {
			memset(mpz_limbs_write(*L[i],nLimbs),0,nLimbs*sizeof(mp_limb_t));
			mpz_clear(*L[i]);
		}
	}
	/* NOTE: a quick look at the gmp source reveals that the return of
	 * mpz_limbs_write is only different than the existing limbs when
	 * the number requested is larger than the allocation (which is
	 * of course larger than mpz_size(X)) */
	return 0;
}
