#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <openssl/err.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+----------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(IV|C) (32 bytes for SHA256) |
 * +------------+--------------------+----------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	/* TODO: write this.  If entropy is given, apply a KDF to it to get
	 * the keys (something like HMAC-SHA512 with KDF_KEY will work).
	 * If entropy is null, just get a random key (you can use the PRF). */
    if (entropy != NULL) {
        unsigned char *bufferO1;
        bufferO1 = malloc(64);
        HMAC(EVP_sha512(), KDF_KEY, 32, entropy, entLen, bufferO1, NULL);
        for (int i = 0; i < 32; i++) {
        	K -> hmacKey[i] = bufferO1[i];
		}
        for (int i = 32; i < 64; i++) {
        	K -> aesKey[i - 32] = bufferO1[i];
		}
        free(bufferO1);
    } else {
      	unsigned char *buffer1, *buffer2;
      	buffer1 = malloc(32);   // allocote bytes in memory
      	randBytes(buffer1, 32); // generate random bytes from buffer
      	for (int i = 0; i < 32; i++) {
        	K->hmacKey[i] = buffer1[i];
		}
        buffer2 = malloc(32);
        randBytes(buffer2, 32);
        for (int i = 0; i < 32; i++) {
            K->aesKey[i] = buffer2[i];
		}
        free(buffer1);
        free(buffer2);
    }
    return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
/* +------------+--------------------+-------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(C) (32 bytes for SHA256) |
 * +------------+--------------------+-------------------------------+
 */
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	/* TODO: finish writing this.  Look at ctr_example() in aes-example.c
	 * for a hint.  Also, be sure to setup a random IV if none was given.
	 * You can assume outBuf has enough space for the result. */
    if (IV == NULL) {
    	for (int i = 0; i < 16; i++) {
			IV[i] = i;
		}
	}
    memcpy(outBuf, IV, 16);
    EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
    if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_ctr(), 0, K->aesKey, IV)) {
        ERR_print_errors_fp(stderr);
	}
    int nwrite;
    if (1 != EVP_EncryptUpdate(ctx, outBuf + 16, &nwrite, inBuf, len)) {
        ERR_print_errors_fp(stderr);
	}
    EVP_CIPHER_CTX_free(ctx);
    int total_bytes = 16 + 32 + nwrite;
    unsigned char myBuf[nwrite];
    memcpy(myBuf, outBuf + 16, nwrite);
    unsigned char *_HMAC = malloc(HM_LEN);
    HMAC(EVP_sha256(), K -> hmacKey, HM_LEN, outBuf, nwrite + 16, _HMAC, NULL);
    memcpy(outBuf + 16 + nwrite, _HMAC, 32);
    return total_bytes; /* TODO: should return number of bytes written, which
	             hopefully matches ske_getOutputLen(...). */
}
size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out){
	/* TODO: write this.  Hint: mmap. */
  if (IV == NULL) {
    for (int i = 0; i < 16; i++) {
      IV[i] = i;
      }
  }

  int fd = open(fnin, O_RDONLY);
  if (fd == -1) return -1;

  struct stat sb;
  if (fstat(fd, &sb) == -1) return -1;
  if (sb.st_size == 0) return -1;
  char *src;
  src = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0); // thanks for the hint :)

  if (src == MAP_FAILED) return -1;
  size_t length = strlen(src) + 1;
  size_t ct_length = ske_getOutputLen(length);
  unsigned char *ct = malloc(ct_length + 1);
  size_t total = ske_encrypt(ct, (unsigned char *)src, length, K, IV);
  int dd = open(fnout, O_CREAT | O_RDWR, S_IRWXU);

  write(dd, ct, (int)total);
  return 0;
}
/* +------------+--------------------+-------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(C) (32 bytes for SHA256) |
 * +------------+--------------------+-------------------------------+
 */
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	/* TODO: write this.  Make sure you check the mac before decypting!
	 * Oh, and also, return -1 if the ciphertext is found invalid.
	 * Otherwise, return the number of bytes written.  See aes-example.c
	 * for how to do basic decryption. */
    unsigned char hmac[32];
    HMAC(EVP_sha256(), K -> hmacKey, HM_LEN, inBuf, len - 32, hmac, NULL);
    for (int i = 0; i < 32; i++) {
        if (hmac[i] != inBuf[len - 32 + i]) return -1;
    }
    unsigned char IV[16];
    for (int i = 0; i < 16; i++) {
    	IV[i] = i;
    }
    int x = len - 32 - 16;
    unsigned char ct[x];
    for (int i = 16; i < 16 + x; i++) {
        ct[i - 16] = inBuf[i];
    }
    EVP_CIPHER_CTX *ctx1 = EVP_CIPHER_CTX_new();
    ctx1 = EVP_CIPHER_CTX_new();
    if (1 != EVP_DecryptInit_ex(ctx1, EVP_aes_256_ctr(), 0, K -> aesKey, IV)) {
        ERR_print_errors_fp(stderr);
	}
    size_t ctLen = x;
    int nwrite = 0;
    if (1 != EVP_DecryptUpdate(ctx1, outBuf, &nwrite, ct, ctLen)) {
        ERR_print_errors_fp(stderr);
	}
    return 0;
}
size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in){
	/* TODO: write this. */
    int fd = open(fnin, O_RDONLY);
    if (fd == -1) return -1;
    struct stat sb;

    if (fstat(fd, &sb) == -1) return -1;
    if (sb.st_size == 0) return -1;

    unsigned char *src;
    src = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

    if (src == MAP_FAILED) return -1;
    char *pt = malloc(sb.st_size - 48);
    ske_decrypt((unsigned char *)pt, src, sb.st_size, K);
    printf("Message after decrypt\n");
    for (int i = 0; i < (sb.st_size - 48); i++){
        printf("%c", pt[i]);
	     }

    printf("\n");
    FILE *f = fopen(fnout, "w");
    fprintf(f, "%s", pt);
    fclose(f);
    return 0;
}
